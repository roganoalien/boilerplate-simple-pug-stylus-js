# Settings
Using gulp to setup everything needed to start the project. *Before* you run **```gulp serve```** for the first time, run **```gulp erase```** to erase .git folder and remove the repository.

## Instructions
1. Clone repository **```git clone git@gitlab.com:roganoalien/boilerplate-simple-pug-stylus-js.git```**
2. Run **```npm install```**
3. After npm do the installs run **```gulp erase```**
4. Create and use this as your repository **```git init```**

###### BEFORE YOU START
Please before you do anything run ```gulp erase```to delete the repository and for you to be able to create your repository.

##### GULP COMMANDS & ACTIONS
###### Remove default repository
```
gulp erase
```
This will erase **this repository folder**, for you to be able to do git init and start your reposity with this configuration.
###### Mount server and proccess the files and folders
```
gulp serve
```
This will run the server and all gulp functions: minifying css, js and images files, creating and sending folders and files to public folder. **After every function has run, it will still be running doing watches for JS files changes or adding other files to scripts/vendors/
###### Watch: assets, main.js, **/*.styl, fonts, vendor/**/*.js
```
gulp watch
```
This task will watch every file added in assets, changes on styls, changes on main.js or files added inside vendor/libs and process it by uglifying it and still watch it.

## Project Setup
This is the folder organization for the project. Where everything will be developed and where everything will be published.
#### App
```
./app
```
This is where all you html, assets, stylus and js will be written.
#### Assets
```
./app/assets
```
Assets is where you'll add every image, video, sounds, svg, etc on your project. Every folder will be sended to ``` ./public/assets ``` 
#### Fonts
```
./app/fonts
```
Font folder inside will be sended to ``` ./public/css/fonts ```. This Setting is already made inside Stylus config folder. As default Roboto Font is added.
#### Stylus
```
./app/stylus
```
Gulp will only render ``` stylus/style.styl ``` file. This is where you'll put every call on every stylus file. 

Every file has to have this nomenclature: **```_filename.styl```**.

Every folder inside ```app/stylus/``` has a *main* file that is called on **```style.styl```** with this nomenclature: **```_main-foldername.styl```** this file is the one that is called in **```style.styl```**
##### Stylus folder arrangement
Stylus folder are made like this:

**``` stylus/config/ ```** *is where every var, mixin, reset, utils, vendor, font, will be arranged*. **``` _main-config.styl ```** *is the file that will be called in* **``` style.styl ```** *. Body will be placed here to add some overall global style.*

**``` stylus/components/ ```** *is where independent components styles are declared; like buttons, links, icons, titles, every object that will not be used just once but twice or more times in the site.*

**```stylus/layout/```** *is where global parts of the site are declared and will be used on every page or in more than one; navigation, headers, footer, widgets.*

**```stylus/pages/```** *is where every specific page style with it own modifications are made.*

#### Scripts
```
./app/scripts/
```
Is where all the **Javascript** is written and inside **```scripts/vendor/libs/```** is where all vendors files will be added. jQuery and other kind of JS libs will be handled inside **```vendor/```** and outside *libs*. ***jQuery 3.0 has already been added***.
#### Views
```
./app/views/
```
Is where all pug files are handled. Every file that is here and not inside other folder will be render so please **DON'T ADD _ before every file name**

**```views/helpers/```** is where all mixins, vars and stuff is written and needs to have **_** for it to not be rendered.

**```views/layout/```** is where all layout global stuff is written, like master layout, navigations, footers, etc. Every file needs to have **_** for it to not be rendered.


