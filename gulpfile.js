var gulp            = require('gulp'),
    autoprefixer    = require('gulp-autoprefixer'),
    cleanCSS        = require('gulp-clean-css'),
    concat          = require('gulp-concat'),
    del             = require('del'),
    plumber         = require('gulp-plumber'),
    pug             = require('gulp-pug'),
    rename          = require("gulp-rename"),
    server          = require('gulp-server-livereload'),
    shell           = require('gulp-shell'),
    stylus          = require('gulp-stylus'),
    uglify          = require('gulp-uglify'),
    watch           = require('gulp-watch');

//-------------------------||
//  REMOVE GIT REPOSITORY  ||
//-------------------------||
//Erase  Repository
gulp.task('erase', function(){
    return del([
        '.git/**/*',
        '.git/'
    ]);
});

//----------------------------------------------------||
//  DELETE EVERY FILE, CREATE FOLDERS AND COPY FILES  ||
//----------------------------------------------------||
//Delete every file from ./public/ before compiling
gulp.task('remove', function(){
    return del([
        'public/**/*'
    ]);
});
//Create folders on ./public
gulp.task('create', ['remove'], function(){
    return gulp.src('*.js', { read: false })
    .pipe(shell([
        'mkdir -p public/assets public/assets/icons public/assets/img public/assets/other public/css public/fonts public/scripts public/scripts/vendor'
    ]));
});
//Copy files from assets and fonts
gulp.task('copy', ['create'], function(){
    gulp.src(['app/assets/**/*'])
        .pipe(gulp.dest('public/assets/'));
    gulp.src(['app/fonts/**/*'])
        .pipe(gulp.dest('public/fonts'));
});
gulp.task('copy-assets', function(){
    gulp.src(['app/assets/**/*'])
        .pipe(gulp.dest('public/assets/'));
    gulp.src(['app/fonts/**/*'])
        .pipe(gulp.dest('public/fonts'));
});
//Globalize remove, create and copy tasks
gulp.task('start', ['remove', 'create', 'copy']);

//--------------------------------------||
//  STYLUS, AUTOPREFIX AND MINIFY TASK  ||
//--------------------------------------||
gulp.task('stylus', ['start'], function(){
    return gulp.src('app/stylus/style.styl')
        .pipe(plumber())
        .pipe(stylus())
        .pipe(autoprefixer({
            browsers: ['> 5%'],
            cascade: false
        }))
        .pipe(cleanCSS({compatibility: 'ie9'}))
        .pipe(rename('style.min.css'))
        .pipe(gulp.dest('public/css'));
});
gulp.task('style-watch', function(){
    return gulp.src('app/stylus/style.styl')
        .pipe(plumber())
        .pipe(stylus())
        .pipe(autoprefixer({
            browsers: ['> 5%'],
            cascade: false
        }))
        .pipe(cleanCSS({compatibility: 'ie9'}))
        .pipe(rename('style.min.css'))
        .pipe(gulp.dest('public/css'));
});
gulp.task('styles', ['stylus']);

//----------------------------------||
//  CONCATENATE VENDORS, UGLIFY JS  ||
//----------------------------------||
gulp.task('concatenate', ['start'], function(){
    return gulp.src(['app/scripts/vendor/jquery-3.2.1.js', 'app/scripts/vendor/libs/**/*.js'])
        .pipe(plumber())
        .pipe(concat('vendor.js'))
        .pipe(uglify())
        .pipe(rename('vendor.min.js'))
        .pipe(gulp.dest('public/scripts/vendor/'));
});
gulp.task('ugly', ['start'], function(){
    return gulp.src('app/scripts/main.js')
        .pipe(plumber())
        .pipe(uglify())
        .pipe(rename('main.min.js'))
        .pipe(gulp.dest('public/scripts/'));
});
gulp.task('ugly-js', function(){
    return gulp.src('app/scripts/main.js')
        .pipe(plumber())
        .pipe(uglify())
        .pipe(rename('main.min.js'))
        .pipe(gulp.dest('public/scripts/'));
});
gulp.task('vendor', function(){
    return gulp.src(['app/scripts/vendor/*.js', 'app/scripts/vendor/libs/**/*.js'])
    .pipe(concat('vendor.js'))
    .pipe(uglify())
    .pipe(rename('vendor.min.js'))
    .pipe(gulp.dest('public/scripts/vendor/'));
});
gulp.task('scripts', ['concatenate', 'ugly']);

//---------||
//  VIEWS  ||
//---------||
gulp.task('views', ['start'], function(){
    return gulp.src('app/views/*.pug')
        .pipe(plumber())
        .pipe(pug())
        .pipe(gulp.dest('public/'));
});
gulp.task('pug', function(){
    return gulp.src('app/views/*.pug')
        .pipe(plumber())
        .pipe(pug())
        .pipe(gulp.dest('public/'));
});

//------------||
//  WATCHERS  ||
//------------||
gulp.task('watch', ['webserver'], function(){
    gulp.watch('app/scripts/*.js', ['ugly-js']);
    gulp.watch('app/stylus/**/*.styl', ['style-watch']);
    gulp.watch('app/views/*.pug', ['pug']);
    watch('app/assets/**/*', function(){
        gulp.start('copy-assets');
    });
    watch('app/scripts/vendor/**/*.js', function(){
        gulp.start('vendor');
    });
});

//-----------------------||
//  LIVERELOAD & SERVER  ||
//-----------------------||
gulp.task('webserver', ['styles', 'scripts', 'views'], function(){
    gulp.src('./public')
        .pipe(server({
            livereload: true,
            defaultFile: 'index.html',
            // directoryListing: {
            //     enable: true,
            //     path: './public'
            // },
            // fallback: 'index.html',
            open: true
        }));
});

//---------||
//  SERVE  ||
//---------||
gulp.task('serve', ['start', 'styles', 'scripts', 'views', 'webserver', 'watch']);