var MYVAR = (function(){
    var _initVars = function _initVars(){};
    var _initEvents = function _initEvents(){};
    return {
        init : function init(){
            _initVars();
            _initEvents();
        }
    }
})();